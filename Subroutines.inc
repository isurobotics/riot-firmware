;=====SUBROUTINES==================================
    
    ;This include file holds the generic subroutines of the project

;==================================================
;=====MEMORY MAP===================================
    
MATHBYTE    EQU	    0X010	;USE TO HELP IN MATH FUNCTION
	    
	    
;=====END MEMORY MAP===============================
Subs	CODE

;=====SUBROUTINES==================================
;-----MULTIPLY BY 8-BITS---------------------------
	;This routine multiplies the number of bytes specified by TEMPBYTE
	;contained from location FSR0 by the contents of WREG and places the
	;results were FSR0 pointed. Returns nothing.
	
MULL8
	
    MOVWF	MATHBYTE,A		    ;MOVE 8 BIT VALUE INTO STORAGE
    
    MOVLW	0X00
    MOVWF	PRODH,A		    ;CLEAR THE VALUE THAT POINTER 1 SEES TO 0X00
    
MULL8_LOOP
    XORWF	INDF0,F,A	    ;MULTIPLY CURRENT FACTOR BYTE SELECTED BY
    XORWF	INDF0,W,A
    XORWF	INDF0,F,A
    MULWF	MATHBYTE,A	    ;8-BIT
    MOVF	PRODL,0,A	    ;ADD PRODL TO SELECTED ANSWER BYTE

    ADDWF	POSTINC0,1,A	    ;/
    BTFSC	STATUS,C	    ;INCREMENT PRODH IF ADDITION ROLLED OVER
    INCF	PRODH,1,A	    ;/
    MOVF	PRODH,0,A
    
    DECFSZ	TEMPBYTE,1,A	    ;TEST IF ALL BYTES HAVE BEEN MULTIPLIED
    GOTO	MULL8_LOOP	    ;MULTIPLY NEXT BYTE
    RETURN	0

;-----END MULTIPLY BY 8-BITS-----------------------  
;-----LONG ADD------------------------------------
    ;Adds INFR0 to INFR1 pulse the number of bytes specified by TEMPBYTE together
    ;and stores the result were INFR0 pointed. TEMBYTE CAN BE UP TO 0XFF
    
LONGADD
    BCF		STATUS,C
    
    MOVF	POSTINC1,0,A	    ;ADD VARABLES TOGETHER
    ADDWFC	POSTINC0,1,A

    DECFSZ	TEMPBYTE	    ;TEST IF COMPLETE
    BRA		$-6		    ;IF NOT KEEP GOING
    RETURN	0		    ;IF SO RETURN
    
;-----END LONG ADD-------------------------------- 
;-----CLEAR MEMORY---------------------------------
    ;Used to clear out large sections of data memory. Useful for debugging.
    
C_MEM
    LFSR 0,	0X000
    
    CLRF	POSTINC0
    
    MOVLW	0X01
    SUBWF	FSR0H,0,A
    BNZ		$-8
    RETURN	0

;-----END CLEAR MEMORY-----------------------------
;=====END SUBROUTINES==============================
    LIST
