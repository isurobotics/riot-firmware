;=====WYATT J CALLISTER============================
    
    ;THIS FILE CONTAINS THE MAIN SETUP FOR ALL THE FUNCTIONS IN THE PROJECT
    

; PIC18F46J53 Configuration Bit Settings

; ASM source line config statements

#include "p18F46J53.inc"

; CONFIG1L
  CONFIG  WDTEN = OFF           ; Watchdog Timer (Disabled - Controlled by SWDTEN bit)
  CONFIG  PLLDIV = 5            ; PLL Prescaler Selection (Divide by 5 (20 MHz oscillator input))
  CONFIG  CFGPLLEN = ON         ; PLL Enable Configuration Bit (PLL Enabled)
  CONFIG  STVREN = OFF          ; Stack Overflow/Underflow Reset (Disabled)
  CONFIG  XINST = OFF           ; Extended Instruction Set (Disabled)

; CONFIG1H
  CONFIG  CPUDIV = OSC3_PLL3    ; CPU System Clock Postscaler (CPU system clock divide by 3)
  CONFIG  CP0 = OFF             ; Code Protect (Program memory is not code-protected)

; CONFIG2L
  CONFIG  OSC = HSPLL           ; Oscillator (HS+PLL, USB-HS+PLL)
  CONFIG  SOSCSEL = HIGH        ; T1OSC/SOSC Power Selection Bits (High Power T1OSC/SOSC circuit selected)
  CONFIG  CLKOEC = OFF          ; EC Clock Out Enable Bit  (CLKO output disabled on the RA6 pin)
  CONFIG  FCMEN = OFF           ; Fail-Safe Clock Monitor (Disabled)
  CONFIG  IESO = ON             ; Internal External Oscillator Switch Over Mode (Enabled)

; CONFIG2H
  CONFIG  WDTPS = 32768         ; Watchdog Postscaler (1:32768)

; CONFIG3L
  CONFIG  DSWDTOSC = INTOSCREF  ; DSWDT Clock Select (DSWDT uses INTRC)
  CONFIG  RTCOSC = INTOSCREF    ; RTCC Clock Select (RTCC uses INTRC)
  CONFIG  DSBOREN = OFF         ; Deep Sleep BOR (Disabled)
  CONFIG  DSWDTEN = OFF         ; Deep Sleep Watchdog Timer (Disabled)
  CONFIG  DSWDTPS = G2          ; Deep Sleep Watchdog Postscaler (1:2,147,483,648 (25.7 days))

; CONFIG3H
  CONFIG  IOL1WAY = ON          ; IOLOCK One-Way Set Enable bit (The IOLOCK bit (PPSCON<0>) can be set once)
  CONFIG  ADCSEL = BIT10        ; ADC 10 or 12 Bit Select (10 - Bit ADC Enabled)
  CONFIG  MSSP7B_EN = MSK7      ; MSSP address masking (7 Bit address masking mode)

; CONFIG4L
  CONFIG  WPFP = PAGE_63        ; Write/Erase Protect Page Start/End Location (Write Protect Program Flash Page 63)
  CONFIG  WPCFG = OFF           ; Write/Erase Protect Configuration Region  (Configuration Words page not erase/write-protected)

; CONFIG4H
  CONFIG  WPDIS = OFF           ; Write Protect Disable bit (WPFP<6:0>/WPEND region ignored)
  CONFIG  WPEND = PAGE_WPFP     ; Write/Erase Protect Region Select bit (valid when WPDIS = 0) (Pages WPFP<6:0> through Configuration Words erase/write protected)
  CONFIG  LS48MHZ = SYS48X8     ; Low Speed USB mode with 48 MHz system clock bit (System clock at 48 MHz USB CLKEN divide-by is set to 8)

;==================================================
;=====VARIBLE SETUP================================
  
I�CADDRESS	EQU	0XFF	;PERMANENTLY SETS I2C ADDRESS (0XFF FOR NORMAL SETUP)
  
;=====END VARIBLE SETUP============================
  
Setup	CODE
    
SETUP
;=====SETUP======================================
    
    BSF		OSCTUNE,PLLEN	    ;SET FOSC TO 48 MHz
 
;-----MEMOROY SETUP------------------------------

    BANKSEL	MATHBYTE
    CLRF	MATHBYTE,1
    CLRF	TASKSEL,1
    CLRF	SSTATUS,1
    CLRF	RCTRACK,1
    CLRF	TEMPBYTE,1
    
    LFSR 2,	0X000

;-----END MEMORY SETUP---------------------------
    
;-----PORT SETUP---------------------------------
    
    BANKSEL	ANCON0
    
    MOVLW	0XC3
    MOVWF	ANCON0    ;ENABLE ANALOG INPUT 2 AND 3, ALL REMAINING ANALOG   
    MOVLW	0X3F
    MOVWF	ANCON1    ;INPUTS ARE DISABLED AND WILL BE DIGITAL I/Os
    
    BSF		INTCON2,7,A ;TURN OFF THE WEEK PULL UP RESISTERS
    
    CLRF	PORTA,A	    ;CLEAR ALL THE RANDOM START UP DATA
    CLRF	PORTB,A
    CLRF	PORTC,A
    CLRF	PORTD,A
    CLRF	PORTE,A	
    
    CLRF	LATA,A	    ;CLEAR ALL THE RANDOM START UP LAT DATA
    CLRF	LATB,A
    CLRF	LATC,A
    CLRF	LATD,A
    CLRF	LATE,A
    
    MOVLW	0X2C  
    MOVWF	TRISA,A	    ;<7:6> OSSILATOR, <3:2> INPUTS, <5:1:0> OUTPUTS
    
    MOVLW	0X00	    ;MOVE IN APPROPIATE TRIS BITS
    BTFSC	TEMPBYTE,0
    MOVLW	0X30	    ;/
    
    MOVWF	TRISB,A	    ;SET PORTB AS ALL INPUTS (UNLESS CONNECTED TO I2C)

    MOVLW	0XC4
    MOVWF	TRISC,A	    ;INPUTS = 7,2	OUTPUTS = 6,1,0
    
    SETF	TRISD,A	    ;SET PORTD AS ALL DIGITAL INPUTS
    
    MOVLW	0X3F
    MOVWF	TRISE,A	    ;
    
;-----END PORT SETUP-----------------------------   
;-----PERIPHERAL PIN SELECT------PG 150-168------
	
; Unlock Registers
        MOVLB       0x0E    ; PPS registers are in BANK 14

        MOVLW       0x55
        MOVWF       EECON2, 0
        MOVLW       0xAA
        MOVWF       EECON2, 0

; Turn off PPS Write Protect BCF PPSCON, IOLOCK, BANKED
	
;----PWM PIN ASSIGNMENTS----
	
        MOVLW       D'14'	;P1A 14 = 0E HEX
        MOVWF       RPOR0, BANKED   ;RA0 = PWM0

        MOVLW       D'18'	;P2B 18 = 12 HEX
        MOVWF       RPOR1, BANKED   ;RA1 = PWM1
	
	MOVLW	    D'22'	;P3A
	MOVWF	    RPOR3
	
	MOVLW	    D'23'	;P3B
	MOVWF	    RPOR4
	
	MOVLW	    D'24'	;P3C
	MOVWF	    RPOR5
	
	MOVLW	    D'25'	;P3D
	MOVWF	    RPOR6

;----SSP2 PIN ASSIGNMENTS----
	
	MOVLW	    D'13'
	MOVWF	    RPINR21	;ASSIGN RC2 (RP13) TO BE MSSP2 SPI DATA IN
	
	MOVLW	    0X0A
	MOVWF	    RPOR11	;ASSIGN RC0 (RP11) TO BE MSSP2 SPI DATA OUT
	
	MOVLW	    0X0B
	MOVWF	    RPOR12	;ASSIGN RC1 (RP12) TO BE MSSP2 SPI CLK
    
;----USART 2 PIN ASSIGNMENT----
	
	MOVLW	    D'18'	;ASSIGN RC7 (RP18) AS THE RX2
	MOVWF	    RPINR16
	
; Lock Registers

        MOVLW       0x55
        MOVWF       EECON2, 0
        MOVLW       0xAA
        MOVWF       EECON2, 0
	
;-----END PERIPHERAL PIN SELECT------------------
;-----USART SETUP--------------------------------
    
    BANKSEL	ODCON2
    CLRF	ODCON2,1
    
    MOVLW       0X00	    ;BAUD RATE SET TO 19.2K WITH   0x00CF
    MOVWF       SPBRGH1,A	    
    MOVLW       0XCF
    MOVWF       SPBRG1,A
    
    BSF         TXSTA1, BRGH
    BSF         BAUDCON1,BRG16
    BCF         TXSTA1, SYNC
    BSF         RCSTA1, SPEN
    BSF         TXSTA1, TXEN
    BCF         RCSTA1, CREN	    ;DON'T RECEIVE
    
    MOVLW       0X02	    ;BAUD RATE SET TO 19.2K WITH   0x026D
    MOVWF       SPBRGH2,A	    
    MOVLW       0X70
    MOVWF       SPBRG2,A
    
    BSF         TXSTA2, BRGH
    BSF         BAUDCON2,BRG16
    BCF         TXSTA2, SYNC
    BSF         RCSTA2, SPEN	
    BCF         TXSTA2, TXEN	    ;DON'T TRANSMIT
    BSF         RCSTA2, CREN
    
;-----END USART SETUP-----------------------------
;-----I2C SETUP-----------------------------------
    
    CLRF	T3CON,A		;MAKE SURE TIMER3 IS OFF UNLESS IT IS NEEDED
    
    MOVLW	0X00		;TEST IF 0X00
    IORLW	I�CADDRESS	;USED TO PROGRAM IN I2C ADDRESS FROM MPLAB
    BZ		I�C_M_SETUP
    INCF	WREG,0		;TEST IF 0XFF
    BNZ		I�C_S_SETUP
    
    CLRF	TBLPTRU,A	;READ IN THE ADDRESS FROM FLASH MEMORY
    MOVLW	0XF8
    MOVWF	TBLPTRH,A
    CLRF	TBLPTRL,A
    
    TBLRD*+
    MOVF	TABLAT,0,A	;/
    
    BZ		I�C_M_SETUP	;TEST IF 0X00 WAS STORED
    
    INCF	WREG,0		;TEST IF 0XFF WAS RECIVED (NO I2C ADDRESS)
    BZ		I�C_SKIP	;IF NO ADDRESS SKIP I2C SETUP
    
    DECF	WREG,0
    
I�C_S_SETUP			;I�C SLAVE SETUP
    
    BSF		SSTATUS,I2CSA,A	;UPDATE STATUS, I2C SLAVE ACTIVE
    
    MOVWF	SSP1ADD,1
    
    BCF		TRISE,1,A	;SET THE I2C ACTIVE BIT
    BSF		PORTE,1,A
    
    MOVLW	0X40
    MOVWF	SSP1STAT,A
    
    MOVLW	0X89
    MOVWF	SSP1CON2,A
    
    MOVLW	0X36		;7-BIT SLAVE, ENABLE MODULE
    MOVWF	SSP1CON1,A
    BRA		I�C_SKIP
 
I�C_M_SETUP			;I�C MASTER SETUP
    
    BSF		SSTATUS,I2CMA,A	;UPDATE STATUS, I2C MASTER MODE ACTIVE
    
    BCF		TRISE,1,A	;SET THE I2C ACTIVE BIT
    BSF		PORTE,1,A
    
    MOVLW	0X27		;OX4F = 100K
    MOVWF	SSP1ADD		;SETS I�C CLOCK RATE

    MOVLW	0X28		;ENABLE I�C HARDWARE, SET AS 7-BIT ADDRESS
    MOVWF	SSP1CON1	;MASTER

;    MOVLW	0X10		;CLEARS, ACKNOWLEDGE STATUS, SETS TO ACKNOLEGE
    CLRF	SSP1CON2	;RECIEVE IDEL, START IDLE, RESTART IDLE, STOP IDLE

    MOVLW	0X80
    MOVWF	SSP1STAT	;SLEW RATE CONTROL ON, SMBus SPECS DISABLED
    
;-----TIMER 3 SETUP------------------------------
;SET UP FOR A .5 SECONDS PER TICK USED FOR INDICATION OF I2C MASTER MODE
    
    MOVLW	0X31
    MOVWF	T3CON,A
;-----END TIMER 3 SETUP--------------------------
    
I�C_SKIP  
    CLRF	TEMPBYTE,A
    TBLRD*			;MOVE THE OTHER SETUPDATA INTO TABLAT
    
;-----END I2C SETUP-------------------------------
;-----PWM SETUP---(ANALOG OUTPUT)-----------------
;----TIMER 4 SETUP----		;TIMER 4 WAS USED TO OPEN UP TIMER 2 FOR THE SPI MODUAL
    MOVLW	0X5A	;SET THE PWM TO FULL RANGE
    MOVWF	PR4,A
    
    BANKSEL	CCPTMRS0
    MOVLW	0X09	;SET THE PWM TO USE TIMER 4
    MOVWF	CCPTMRS0,1
    
;----PWM 1------------
    CLRF        CCPR1L,A
	
    MOVLW	0X0C	;SINGLE OUTPUT, PWM LSBs = 11, PWM MODE ACTIVE HIGH
    MOVWF	CCP1CON,A

;----PWM 2------------
    CLRF        CCPR2L,A	;START WITH NO PULSE WIDTH
	
    MOVLW	0X0C	;SINGLE OUTPUT, PWM LSBs = 11, PWM MODE ACTIVE HIGH
    MOVWF	CCP2CON,A

    MOVLW	0X04
    MOVWF	T4CON,A
	
;-----END PWM SETUP---(ANALOG OUTPUT)------------
;-----A/D SETUP----------------------------------
	
    MOVLW	0X08
    MOVWF	ADCON0	    ;SET ANALOG CHANNEL 2 AS THE DEFULT
    
    MOVLW	0X2E	    ;LEFT JUSTIFIY, TAD = 12, FOSC/64
    MOVWF	ADCON1
    
    BSF		ADCON1,ADON,A	;TURN ON THE MODULE
    BSF		ADCON1,1,A	;START THE FIRST CONVERTION
    
;-----END A/D SETUP------------------------------
;-----TIMER 0 SETUP------------------------------
    
    CLRF	TMR0H,A	    ;AS LONG AS NO READING OF TIMER 0 HAPPENS CLRF TMR0L
			    ;WILL CLEAR OUT ALL OF TMR0 TO PREVENT NC_INT
    CLRF	TMR0L,A

    MOVLW	0X85	    ;TIMER 0 ON, PRESCALE 1:64, 16 BIT
    MOVWF	T0CON,A	    ; ~.25 SECONDS TILL OVER FLOW
    
;-----END TIMER 0 SETUP--------------------------
    RETURN	0

;-----END SETUP----------------------------------
    
;-----I2C ADDRESS STORE--------------------------
    
I�C_ADD_SET
    
    BTFSC	PORTC,2,A	;TEST IF SPI CLOCK IS TIED TO SPI DIN
    RETURN	0		;THIS SITUATION WILL REPROGRAM I2C ADDRESS
    BSF		PORTC,1,A
    BTFSS	PORTC,2,A
    RETURN	0

    MOVF	PORTD,0,A	;WRITE DIGITAL INPUTS TO OUTPUTS FOR VARIFICATION
    MOVWF	LATB,A
    
    BTFSC	LATB,0,A	;WAIT FOR THE SIGNAL TO SAVE PORT B AS THE ADDRESS
    BRA		$-6
    
    CLRF	TBLPTRU,A	;SET THE ADDRESS TO ERASE
    MOVLW	0XF8
    MOVWF	TBLPTRH,A
    CLRF	TBLPTRL,A
    
    BCF		INTCON,GIEH,A
    BSF		EECON1,WREN,A	;PREPARE FOR WRITING TO THE FLASH MEMORY
    BSF		EECON1,FREE,A
    
    MOVLW	0X55		;CHARGE PUMP!!!
    MOVWF	EECON2,A
    MOVLW	0XAA
    MOVWF	EECON2,A	;/  
    BSF		EECON1,WR	;START ERASE FUNCTION
    
    MOVLW	0XFE		;TEST IF DISABLING THE I2C FUNCTION
    SUBWF	LATB,0,A
    BNZ		$+4
    INCF	LATB,1,A	;DISABLING I2C
    MOVF	LATB,0,A	;ENABLING I2C
    
    MOVWF	TABLAT,A	;MOVE DATA TO THE TABLE LATCH
    TBLWT*+			;MOVE DATA INTO THE TABLE
    
    CLRF	PORTB,A		;CLEAR THE OUTPUTS TO SIGNAL, READY FOR NEXT BYTE
    
    BTFSS	PORTD,A		;WAIT FOR COMMAND BUTTON TO BE RELEASED
    BRA		$-2 
    
    MOVLW	0X7F		;TURN ON TIMER 2
    MOVWF	T2CON,A
    
    BCF		PIR1,TMR2IF,A	;SETUP A WAITING ROUTINE TO HELP WITH DEBOUNCING
    BTFSS	PIR1,TMR2IF,A
    BRA		$-2
    
    BSF		STATUS,C,A
    RLCF	LATB,1,A
    BTFSS	STATUS,C,A
    BRA		$-0X0C		;/  
    
    COMF	PORTD,0,A	;WRITE THE INPUTS TO THE OUTPUTS
    MOVWF	LATB,A
    
    BTFSC	PORTD,0,A	;WAIT FOR THE SIGNAL TO SAVE PORT B AS CONFIGURATION
    BRA		$-6
    
    MOVWF	TABLAT,A
    TBLWT*
    
    BTFSS	PORTD,0,A
    BRA		$-2
    
    BSF		EECON1,WPROG,A	;SETUP WRITE OPERATION
    BSF		EECON1,WREN,A	;/
    
    MOVLW	0X55		;CHARGE PUMP!!!
    MOVWF	EECON2,A
    MOVLW	0XAA
    MOVWF	EECON2,A   	;/  
    BSF		EECON1,WR,A	;WRITE TO PROGRAM MEMORY
    
    BCF		EECON1,WREN,A
    
    BCF		TRISE,1,A	;TEST CODE
    BCF		PORTE,1,A
    BTFSC	EECON1,WRERR,A
    BSF		PORTE,1,A	;/
    
I2C_RESULT
    BTFSC	PIR1,TMR2IF,A
    INCF	LATD,1,A
    BTFSC	PIR1,TMR2IF,A
    BCF		PIR1,TMR2IF,A
    
    BTFSS	LATD,7,A
    BRA		I2C_SET_END
    
    CLRF	LATD,A
    BTG		TEMPBYTE,0
    
    BTFSC	TEMPBYTE,0
    CLRF	TBLPTRL,A	;READ IN THE STORED VALUE
    BTFSS	TEMPBYTE,0
    INCF	TBLPTRL,1,A
    
    TBLRD*
    
    MOVF	TABLAT,0,A
    MOVWF	LATB,A		;/
    
I2C_SET_END
    BTFSC	PORTC,2,A	;WAIT FOR THE TIE TO BE REMOVED
    BRA		I2C_RESULT
    
    RESET			;RESET THE MICROCONTROLLER AND APPLY THE NEW ADDRESS
    
;-----END I2C ADDRESS STORE----------------------