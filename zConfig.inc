;=====CONFIGURATION================================
    ;This .inc file contains the default configuration when the PIC is programed
    
    ;September 24,2016
    ;Created the configuration file. At this time there are only two bytes of
    ;configuration for the Ry@ firmware, so this should be pretty short
    
;==================================================
;=====START CONFIG=================================
    CONFIG	0XF800,	    0X00FF	    ;STORE 0X00 AS THE DISCREET PLAYBACK, AND 0XFF AS THE I2C ADDRES
    
    
;=====END CONFIG===================================

