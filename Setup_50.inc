;=====WYATT J CALLISTER============================
    
    
; PIC18F46J50 Configuration Bit Settings

; ASM source line config statements

#include "p18F46J50.inc"
    
;THIS FILE CONTAINS THE MAIN SETUP FOR ALL THE FUNCTIONS IN THE PROJECT
    
; CONFIG1L
  CONFIG  WDTEN = OFF           ; Watchdog Timer (Disabled - Controlled by SWDTEN bit)
  CONFIG  PLLDIV = 5            ; PLL Prescaler Selection bits (Divide by 5 (20 MHz oscillator input))
  CONFIG  STVREN = OFF          ; Stack Overflow/Underflow Reset (Disabled)
  CONFIG  XINST = OFF           ; Extended Instruction Set (Disabled)

; CONFIG1H
  CONFIG  CPUDIV = OSC1         ; CPU System Clock Postscaler (No CPU system clock divide)
  CONFIG  CP0 = OFF             ; Code Protect (Program memory is not code-protected)

; CONFIG2L
  CONFIG  OSC = HSPLL           ; Oscillator (HS+PLL, USB-HS+PLL)
  CONFIG  T1DIG = OFF           ; T1OSCEN Enforcement (Secondary Oscillator clock source may not be selected)
  CONFIG  LPT1OSC = ON          ; Low-Power Timer1 Oscillator (Low-power operation)
  CONFIG  FCMEN = OFF           ; Fail-Safe Clock Monitor (Disabled)
  CONFIG  IESO = ON             ; Internal External Oscillator Switch Over Mode (Enabled)

; CONFIG2H
  CONFIG  WDTPS = 32768         ; Watchdog Postscaler (1:32768)

; CONFIG3L
  CONFIG  DSWDTOSC = INTOSCREF  ; DSWDT Clock Select (DSWDT uses INTRC)
  CONFIG  RTCOSC = INTOSCREF    ; RTCC Clock Select (RTCC uses INTRC)
  CONFIG  DSBOREN = OFF         ; Deep Sleep BOR (Disabled)
  CONFIG  DSWDTEN = OFF         ; Deep Sleep Watchdog Timer (Disabled)
  CONFIG  DSWDTPS = G2          ; Deep Sleep Watchdog Postscaler (1:2,147,483,648 (25.7 days))

; CONFIG3H
  CONFIG  IOL1WAY = ON          ; IOLOCK One-Way Set Enable bit (The IOLOCK bit (PPSCON<0>) can be set once)
  CONFIG  MSSP7B_EN = MSK7      ; MSSP address masking (7 Bit address masking mode)

; CONFIG4L
  CONFIG  WPFP = PAGE_63        ; Write/Erase Protect Page Start/End Location (Write Protect Program Flash Page 63)
  CONFIG  WPEND = PAGE_WPFP     ; Write/Erase Protect Region Select (valid when WPDIS = 0) (Page WPFP<5:0> through Configuration Words erase/write protected)
  CONFIG  WPCFG = OFF           ; Write/Erase Protect Configuration Region (Configuration Words page not erase/write-protected)

; CONFIG4H
  CONFIG  WPDIS = OFF           ; Write Protect Disable bit (WPFP<5:0>/WPEND region ignored)

;================================================
;==================================================
    
Setup	CODE
    
SETUP
;=====SETUP======================================
    
    MOVLB	0X01
    BSF		OSCTUNE,PLLEN	    ;SET FOSC TO 48 MHz
 
;-----MEMOROY SETUP------------------------------


;-----END MEMORY SETUP---------------------------
;-----PORT SETUP---------------------------------
    
    BANKSEL	ANCON0
    MOVLW	0XF3
    MOVWF	ANCON0    ;ENABLE ANALOG INPUT 2 AND 3, ALL REMAINING ANALOG   
    MOVLW	0X3F
    MOVWF	ANCON1    ;INPUTS ARE DISABLED AND WILL BE DIGITAL I/Os
    
    BSF		INTCON2,7,A ;TURN OFF THE WEEK PULL UP RESISTERS
    
    CLRF	PORTA,A	    ;CLEAR ALL THE RANDOM START UP DATA
    CLRF	PORTB,A
    CLRF	PORTC,A
    CLRF	PORTD,A
    CLRF	PORTE,A	
    
    CLRF	LATA,A	    ;CLEAR ALL THE RANDOM START UP LAT DATA
    CLRF	LATB,A
    CLRF	LATC,A
    CLRF	LATD,A
    CLRF	LATE,A
    
    MOVLW	0X0C  
    MOVWF	TRISA,A	    ;<7:6> OSSILATOR, <3:2> INPUTS, <5:1:0> OUTPUTS
    
    SETF	TRISB,A	    ;SET PORTB AS ALL INPUTS

    MOVLW	0X84
    MOVWF	TRISC,A	    ;INPUTS = 7,2	OUTPUTS = 6,1,0
    
    CLRF	TRISD,A	    ;SET PORTD AS ALL DIGITAL OUTPUTS
    
    MOVLW	0X04
    MOVWF	TRISE,A	    ;<2> USB DETECT INPUT, <1:0> STATUS PUTPUT INDICATORS
    
;-----END PORT SETUP-----------------------------   
;-----PERIPHERAL PIN SELECT------PG 150-168------
	
; Unlock Registers
        MOVLB       0x0E    ; PPS registers are in BANK 14

        MOVLW       0x55
        MOVWF       EECON2, 0
        MOVLW       0xAA
        MOVWF       EECON2, 0

; Turn off PPS Write Protect BCF PPSCON, IOLOCK, BANKED
	
;-----PWM PIN ASSIGNMENTS------
	
        MOVLW       0x0E	;P1A 14 = 0E HEX
        MOVWF       RPOR0, BANKED   ;RA0 = PWM0

        MOVLW       0x12	;P2B 18 = 12 HEX
        MOVWF       RPOR1, BANKED   ;RA1 = PWM1

;-----SSP2 PIN ASSIGNMENTS-----
	
	MOVLW	    D'13'
	MOVWF	    RPINR21	;ASSIGN RC2 (RP13) TO BE MSSP2 SPI DATA IN
	
	MOVLW	    0X09
	MOVWF	    RPOR11	;ASSIGN RC0 (RP11) TO BE MSSP2 SPI DATA OUT
	
	MOVLW	    0X0A
	MOVWF	    RPOR12	;ASSIGN RC1 (RP12) TO BE MSSP2 SPI CLK
    
; Lock Registers

        MOVLW       0x55
        MOVWF       EECON2, 0
        MOVLW       0xAA
        MOVWF       EECON2, 0
	
;-----END PERIPHERAL PIN SELECT------------------
;-----SERIAL SETUP-------------------------------
    
    MOVLW       0X00	    ;BAUD RATE SET TO 115.2K WITH   0x0067
    MOVWF       SPBRGH	    ;BAUD RATE SET TO 9600   WITH   0x04E1
    MOVLW       0x67
    MOVWF       SPBRG
    BSF         TXSTA1, BRGH
    BSF         BAUDCON1,BRG16
    BCF         TXSTA1, SYNC
    BSF         RCSTA1, SPEN
    BSF         TXSTA1, TXEN
    BSF         PIE1,   RC1IE
    BSF         RCSTA1, CREN
    MOVLW       0X0F
	
;-----END SERIAL SETUP----------------------------
;-----PWM SETUP---(ANALOG OUTPUT)-----------------
;----TIMER 4 SETUP----		;TIMER 4 WAS USED TO OPEN UP TIMER 2 FOR THE SPI MODUAL
    MOVLW	0X3F	;SET THE PWM TO FULL RANGE
    MOVWF	PR4,A
	
    BANKSEL	TCLKCON
    BSF		TCLKCON,1,1	;SET THE PWM TO USE TIMER 4
	
;----PWM 1------------
    CLRF        CCPR1L,A
	
    MOVLW	0X3C	;SINGLE OUTPUT, PWM LSBs = 11, PWM MODE ACTIVE HIGH
    MOVWF	CCP1CON,A

;----PWM 2------------
    CLRF        CCPR2L,A	;START WITH NO PULSE WIDTH
	
    MOVLW	0X3C	;SINGLE OUTPUT, PWM LSBs = 11, PWM MODE ACTIVE HIGH
    MOVWF	CCP2CON,A

    MOVLW	0X44
    MOVWF	T4CON,A
	
;-----END PWM SETUP---(ANALOG OUTPUT)------------
    
;-----A/D SETUP----------------------------------
	
    MOVLW	0X01	    ;ENABLE A/D MODULE
    MOVWF	ADCON0
    BSF		ADCON1,CHS0 ;SET ANALOG CHANNEL 2 AS THE DEFULT
    
    MOVLW	0X2D	    ;LEFT JUSTIFIY, TAD = 12, FOSC/32
    MOVWF	ADCON1
    
    BSF		ADCON1,1
    
;-----END A/D SETUP------------------------------
    
    RETURN	0

    LIST